'use strict'
//1、条件判断

/*
if (){

}else{

}
*/


/*
if(){

}else if(){

}else{

}
*/


//条件判断的顺序问题

// var age=20;

// if(6<age){
//     console.log('6小于20')
// }else if(18<age){
//     console.log('18小于20')
// }

// var height =parseFloat(prompt('请输入你的身高（单位：米）'));
// var weight =parseFloat(prompt('请输入你的体重（单位：千克）'));

// var bmi=weight/(height*height);

// if(bmi<18.5){
//     console.log('过轻')
// }else if(bmi>=18.5 && bmi<25){
//     console.log('正常')
// }else if(bmi>=25 && bmi<28){
//     console.log('过重')
// }else if(bmi>=28 && bmi<32){
//     console.log('肥胖')
// }else if(bmi>=32){
//     console.log('高于32：严重肥胖')
// }

/*
根据BMI公式（体重除以身高的平方）帮小明计算他的BMI指数，并根据BMI指数
低于18.5：过轻
18.5-25：正常
25-28：过重
28-32：肥胖
高于32：严重肥胖

*/

//2、循环

/*
for()

for .. in //要过滤掉对象继承的属性，用hasOwnProperty()来实现

while

do while

循环的边界
*/

// var o = {
//     name: 'Jack',
//     age: 20,
//     city: 'Beijing'
// };

// for(var key in o){
//     console.log(key);
//     console.log(o[key]);
// }

// var a = ['A', 'B', 'C'];
// for (var i in a) {
//     console.log(i); // '0', '1', '2'
//     console.log(a[i]); // 'A', 'B', 'C'
// }

// var AGE=12;
// while(AGE>0){
//     console.log(AGE);
//     AGE-=1;//AGE=AGE-1;
// }

// var ok=0;
// do{
//     console.log('山里有座庙');
// }while(ok>0)


//3、Map 是一组键值对的结构，具有极快的查找速度。

// var m = new Map([['Michael', 95], ['Bob', 75], ['Tracy', 85]]);
// m.get('Michael'); // 95
// m.set('蔡文姬','史上最强蔡文姬');

// console.log(m.get('蔡文姬'));

// //redis     
// var m_New=new Map();
// m_New.set('李元芳','萌萌哒');
// m_New.delete('李元芳');

//4、Set 和Map类似，也是一组key的集合，但不存储value。由于key不能重复，所以，在Set中，没有重复的key

// var s1 = new Set(); // 空Set
// var s2 = new Set([1, 2, 3]); // 含1, 2, 3


//5、iterable 遍历Array可以采用下标循环，
//    遍历Map和Set就无法使用下标。为了统一集合类型，ES6标准引入了新的iterable类型，Array、Map和Set都属于iterable类型。

//具有iterable类型的集合可以通过新的for ... of循环来遍历

// var a = ['A', 'B', 'C'];
// var s = new Set(['A', 'B', 'C']);
// var m = new Map([[1, 'x'], [2, 'y'], [3, 'z']]);
// for (var x of a) { // 遍历Array
//     console.log(x);
// }
// for (var x of s) { // 遍历Set
//     console.log(x);
// }

// for (var x of m) { // 遍历Map
//     console.log(x[0] + '=' + x[1]);
// }

/*
for ... of循环和for ... in循环有何区别？

for ... in循环由于历史遗留问题，它遍历的实际上是对象的属性名称。一个Array数组实际上也是一个对象，它的每个元素的索引被视为一个属性。

当我们手动给Array对象添加了额外的属性后，for ... in循环将带来意想不到的意外效果


*/

var a = ['A', 'B', 'C'];
a.name = 'Hello';

for (var x in a) {
    console.log(x); // '0', '1', '2', 'name'
}

for(var x of a){
    console.log(x); // 'A', 'B', 'C'
}

console.log(a.length);


//forEach回调函数及参数含义

// //数组
// a.forEach((item,index,arr)=>{
//     console.log(item +', index='+index);
//     console.log('---------------------------------------')
//     console.log(arr[index]);
// })

// //set
// var s = new Set(['A', 'B', 'C']);
// s.forEach(function (element, sameElement, set) {
//     console.log(element);
//     console.log('=======================================')
//     console.log(sameElement);
// });

// //map
// var m = new Map([[1, 'x'], [2, 'y'], [3, 'z']]);
// m.forEach(function (value, key, map) {
//     console.log(value);
// });


var a = ['A', 'B', 'C'];
var s = new Set(['A', 'B', 'C']);
var m = new Map([[1, 'x'], [2, 'y'], [3, 'z']]);


a.forEach((item,index)=>{
    console.log(item);
})