'use strict'
//数据类型



//number

var x=1;
var y=0.23;
var z=-0.78;

console.log(x);


var i=x+y;
console.log(i);


console.log(3/2);

console.log(8*9);

console.log(34+15);


//字符串 是以'或"包围起来的一串字符


var aaa='这是一个字符串';
var bbb="这也是一个字符串";

console.log(aaa);
console.log(bbb);


//布尔值 true false


// console.log(true);

// console.log(false);

// &&、||、 !

console.log(true && true);//true

console.log(true && false);//false

console.log(true || true);//true
console.log(true || false);//true

console.log(!true);//false

console.log(!false);//true


console.log(2>1);//true

console.log(3!==3);//fasle


//等于  ==   ====


// 第一种是转换后比较

// 第二种是先比较数据类型，再比较值


var Ab='3';
var aB=3;


console.log(Ab==aB);//true
console.log(Ab===aB);//false







//null undefined



// null表示值还未赋值


// undefined表示变量不存在

// var cctv

// console.log(cctv);


// console.log(ccav);


//数组 


var arr=[1,2,'bbcbde',8.078998];





console.log(arr[0]);

console.log(arr[2]);

console.log(arr[3]);


var kko=new Array(3);

kko.push(5);

kko.push(89);

kko.forEach(element => {
    console.log(element);
});

console.log(kko.pop());

kko.forEach(en=>{
    console.log(en);
})



//对象

/*

public class StudentInfo(
    private int _age;

    public int Age {
        get
        {
            return _age;
        }
        set{
            _age=values;
        }
    }
)



var x=new StudentInfo();
*/


var studentInfo={
    Age:80,
    Name:'Hu',
    Course:[{
        courseName:'JavaScript'
    },{
        courseName:'Sql高级'
    }]
};

console.log(studentInfo.Age);

console.log(studentInfo.Course);


//变量


 var xxx='xxx';

console.log(xxx);


//strict模式


var yy=xxx;



//除null、undefined、NaN、0、'' 以外，其他的任何值都 被认为是true
if(2){
    console.log('这是true');
}


if(xxx){
    console.log('你好中国')
}

var str='这是一个字符串';

var str1="I'm String";

//转义符 \   

var str2="I'm \n \"lazy\",How are you?\n；                              "

var str3=`别搞有些没的,啊
范三样，昨天的诗词好吃不？？
味道 不错`

var str4=`我信你个鬼……
糟老头子，
我的风骚你不懂……`


var str5=`<div>中华人民共和国</div>
<div>132456</div>`
var hh=document.getElementById('hh');
document.write(str5);
// str3[7]='TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT'
console.log(str3[7]);


// console.log(str2.toUpperCase());

// console.log(str2.toLowerCase());

// console.log(str2.toLocaleLowerCase());

// console.log(str2.substring(0,10));

// console.log(str2);



//数组


var arr1=[1,2,6,3,4,5];

console.log(arr1);

arr1.push(8);

console.log(arr1);

console.log(arr1.pop());



// arr1.unshift(9);

console.log(arr1);

var arr2=arr1.slice(1,3);//包含开始下标，不包含结束下标

console.log(arr2);

console.log(arr1.slice());

console.log(arr1.indexOf(5));
console.log(arr1.indexOf(9));

console.log(str3.indexOf('啊'));

// console.log(arr1.sort());

console.log(arr1.reverse());


//splice
console.log(arr1.splice(2));

console.log(arr1);

console.log(arr1.splice(0,0,0));

console.log(arr1);


//concact join

var x=[1,2,3,4];
var y=['x','y','z'];
var z=[['a','b','c'],[9,0]];

console.log(x.concat(y,z));
console.log(x.concat(y,['a','b'],[55,32,78]));

console.log(x.join('_'));

var weight=parseInt( prompt('请输入体重',0));
console.log(weight);


//BMI  系数 身高除于体重的两倍


